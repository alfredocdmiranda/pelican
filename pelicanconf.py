#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Alfredo'
SITENAME = 'IO Project'
SITESUBTITLE = u'Blog about arduino, home automation and all my personal projects.'
SITEURL = ''

THEME = 'theme'

PATH = 'content'
OUTPUT_PATH = 'public'

DEFAULT_DATE = 'fs'
DEFAULT_DATE_FORMAT = '%d %b %Y'
TIMEZONE = 'Europe/Lisbon'

DEFAULT_LANG = 'en'

# Post and Pages path
ARTICLE_URL = '{date:%Y}/{date:%m}/{slug}.html'
ARTICLE_SAVE_AS = '{date:%Y}/{date:%m}/{slug}.html'
PAGE_URL = 'pages/{slug}/'
PAGE_SAVE_AS = 'pages/{slug}/index.html'
YEAR_ARCHIVE_SAVE_AS = '{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = '{date:%Y}/{date:%m}/index.html'

# Tags and Category path
CATEGORY_URL = 'category/{slug}'
CATEGORY_SAVE_AS = 'category/{slug}/index.html'
CATEGORIES_SAVE_AS = 'catgegories.html'
TAG_URL = 'tag/{slug}'
TAG_SAVE_AS = 'tag/{slug}/index.html'
TAGS_SAVE_AS = 'tags.html'

# Author
AUTHOR_URL = 'author/{slug}'
AUTHOR_SAVE_AS = 'author/{slug}/index.html'
AUTHORS_SAVE_AS = 'authors.html'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
# LINKS = (('Pelican', 'http://getpelican.com/'),
#          ('Python.org', 'http://python.org/'),
#          ('Jinja2', 'http://jinja.pocoo.org/'),
#          ('You can modify those links in your config file', '#'),)

# Social widget
# SOCIAL = (
#     ('twitter', 'https://twitter.com/alfredocmn'),
#     ('github', 'https://github.com/myalfredocdmiranda'),
# )

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

### Theme Specific Settings ###
# HOME_COVER = 'https://casper.ghost.org/v1.0.0/images/welcome.jpg'
HOME_COLOR = 'black'

AUTHORS_BIO = {
  "alfredo": {
    "name": "Alfredo Miranda",
    "image": "images/avatar.jpg",
    "website": "https://www.ioproject.com.br",
    "linkedin": "alfredocdmiranda",
    "github": "alfredocdmiranda",
    "location": "Portugal",
    "bio": "Brazilian computer engineer, who loves technology, mostly technology that improve lives. Passionate about home automation and how things work."
  }
}

JINJA_ENVIRONMENT = {
  'extensions' :[
    'jinja2.ext.loopcontrols',
    'jinja2.ext.i18n',
    'jinja2.ext.with_',
    'jinja2.ext.do'
  ]
}

# JINJA_FILTERS = {'max': max}