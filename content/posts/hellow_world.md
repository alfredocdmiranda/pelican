Title: Hello World
Date: 2020-11-27
Category: 
Tags: 
Slug: hello-world
Color: black

Hello World,

Long time since last time I posted anything. You must be wondering "What do you mean?! There is nothing here!". Well, the blog has already gone through some iterations where I published it on Wordpress, on my own server (and in different languages), however, I have lost all the content I previous published. Then, let's try one more time and try not to lose everything again and try to keep posting regularly.

The blog is totaly new, but the idea is the same. I want to post my personal evolution on some subjects (tech related) and try to fill some information gaps that myself can't find easily.

I hope everyone likes the content and that it helps you in some way.

So long, and thanks for all the fish!